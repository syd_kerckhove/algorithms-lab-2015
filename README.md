# Algorithms Lab


## Usage

When in the directory for a particular problem, the following command will run the solution:
```
../run.sh
```
